import task3 from './task3';
import {
  testArray1,
  testArray2,
  testArray3,
  testArray4,
} from '../index';

describe('Task 3', () => {
  test(`Новий масив: [10, 30, 16, 80, 24]`, () => {
    expect(task3(testArray1))
        .toEqual(expect.arrayContaining([10, 30, 16, 80, 24]));
  });
  test(`Новий масив: [18, 10, 20, 64, 11, 70]`, () => {
    expect(task3(testArray2))
        .toEqual(expect.arrayContaining([18, 10, 20, 64, 11, 70]));
  });
  test(`Новий масив: [100, 21, 30, 34, 0]`, () => {
    expect(task3(testArray3))
        .toEqual(expect.arrayContaining([100, 21, 30, 34, 0]));
  });
  test(`Новий масив: []`, () => {
    expect(task3(testArray4))
        .toEqual(expect.arrayContaining([]));
  });
});

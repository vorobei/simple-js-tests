import task2 from './task2';
import {
  testArray1,
  testArray2,
  testArray3,
  testArray4,
} from '../index';

describe('Task 2', () => {
  test(`Сума лише додатніх елементів testArray1 дорівнює 160`, () => {
    expect(task2(testArray1)).toBe(160);
  });
  test(`Сума лише додатніх елементів testArray2 дорівнює 193`, () => {
    expect(task2(testArray2)).toBe(193);
  });
  test(`Сума лише додатніх елементів testArray3 дорівнює 185`, () => {
    expect(task2(testArray3)).toBe(185);
  });
  test(`Сума лише додатніх елементів testArray4 дорівнює 0`, () => {
    expect(task2(testArray4)).toBe(0);
  });
});

import task5 from './task5';
import {
  testArray1,
  testArray2,
  testArray3,
  testArray4,
} from '../index';

describe('Task 5', () => {
  test(`Результат для testArray1: 10`, () => {
    expect(task5(testArray1)).toBe(10);
  });
  test(`Результат для testArray2: 10`, () => {
    expect(task5(testArray2)).toBe(10);
  });
  test(`Результат для testArray3: 21`, () => {
    expect(task5(testArray3)).toBe(21);
  });
  test(`Результат для testArray4: false`, () => {
    expect(task5(testArray4)).toBe(false);
  });
});

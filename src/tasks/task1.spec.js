import task1 from './task1';
import {
  testArray1,
  testArray2,
  testArray3,
  testArray4,
} from '../index';

describe('Task 1', () => {
  test('Сума елементів testArray1 дорівнює 130', () => {
    expect(task1(testArray1)).toBe(130);
  });
  test('Сума елементів testArray2 дорівнює 193', () => {
    expect(task1(testArray2)).toBe(193);
  });
  test('Сума елементів testArray3 дорівнює 143', () => {
    expect(task1(testArray3)).toBe(143);
  });
  test('Сума елементів testArray4 дорівнює -224', () => {
    expect(task1(testArray4)).toBe(-224);
  });
});

import task4 from './task4';
import {
  testArray1,
  testArray2,
  testArray3,
  testArray4,
} from '../index';

describe('Task 4', () => {
  test(`Новий масив: [-10, -30, 30, -16, -80, -24]`, () => {
    expect(task4(testArray1))
        .toEqual(expect.arrayContaining([-10, -30, 30, -16, -80, -24]));
  });
  test(`Новий масив: [-18, -10, -20, -64, -11, -70]`, () => {
    expect(task4(testArray2))
        .toEqual(expect.arrayContaining([-18, -10, -20, -64, -11, -70]));
  });
  test(`Новий масив: [30, -100, -21, -30, 12, -34]`, () => {
    expect(task4(testArray3))
        .toEqual(expect.arrayContaining([30, -100, -21, -30, 12, -34]));
  });
  test(`Новий масив: [20, 70, 22, 60, 22, 30]`, () => {
    expect(task4(testArray4))
        .toEqual(expect.arrayContaining([20, 70, 22, 60, 22, 30]));
  });
});

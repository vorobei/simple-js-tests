import task1 from './tasks/task1';
import task2 from './tasks/task2';
import task3 from './tasks/task3';
import task4 from './tasks/task4';
import task5 from './tasks/task5';

export const testArray1 = [10, 30, -30, 16, 80, 24];
export const testArray2 = [18, 10, 20, 64, 11, 70];
export const testArray3 = [-30, 100, 21, 30, -12, 34, 0];
export const testArray4 = [-20, -70, -22, -60, -22, -30];

// console.log('Task 1 ------------------');
// console.log(task1(testArray1));
// console.log(task1(testArray2));
// console.log(task1(testArray3));
// console.log(task1(testArray4));

// console.log('Task 2 ------------------');
// console.log(task2(testArray1));
// console.log(task2(testArray2));
// console.log(task2(testArray3));
// console.log(task2(testArray4));
//
// console.log('Task 3 ------------------');
// console.log(task3(testArray1));
// console.log(task3(testArray2));
// console.log(task3(testArray3));
// console.log(task3(testArray4));

// console.log('Task 4 ------------------');
// console.log(task4(testArray1));
// console.log(task4(testArray2));
// console.log(task4(testArray3));
// console.log(task4(testArray4));
//
// console.log('Task 5 ------------------');
// console.log(task5(testArray1));
// console.log(task5(testArray2));
// console.log(task5(testArray3));
// console.log(task5(testArray4));
//

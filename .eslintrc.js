module.exports = {
  parser: '@babel/eslint-parser',
  parserOptions: {
    babelOptions: {
      configFile: './babel.config.json',
    },
  },
  env: {
    'browser': true,
    'node': true,
    'es6': true,
    'jest/globals': true,
  },
  plugins: ['jest'],
  extends: ['eslint:recommended', 'google'],
  rules: {
    'require-jsdoc': 0,
    'operator-linebreak': 0,
    'object-curly-spacing': 0,
    'no-debugger': 0,
    'no-unused-vars': 0,
    'linebreak-style': ['error', 'windows'],
    'jest/no-disabled-tests': 'warn',
    'jest/no-focused-tests': 'error',
    'jest/no-identical-title': 'error',
    'jest/prefer-to-have-length': 'warn',
    'jest/valid-expect': 'error',
  },
};
